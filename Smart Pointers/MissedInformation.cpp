#include "MissedInformation.h"
#include <iostream>

MissedInformation::MissedInformation()
{
	std::cout << "I unfortunately could not do much with weak pointers... \n";
}

MissedInformation::~MissedInformation()
{
	std::cout << "The announcement that was supposed to help never came and I could not find anything on google... \n";
}
