#pragma once
#include <string>

class Human
{
public:
	Human();
	~Human();
	void Talk();
	void Cry();
	std::string name;
};

