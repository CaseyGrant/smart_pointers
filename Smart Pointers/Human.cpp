#include "Human.h"
#include <iostream>

Human::Human()
{
	std::cout << "New life has been created!\n";
}

Human::~Human()
{
	std::cout << "The life form is no longer with us...\n";
}

void Human::Talk()
{
	std::cout << name <<" Says: Eat a pickle\n";
}

void Human::Cry()
{
	std::cout << name << " starts crying...\n";
}
