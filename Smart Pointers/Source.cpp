#include "Human.h" // allows the use of human
#include "MissedInformation.h" // allows the use of missed information
#include <iostream> // allows the use of cout and cin

int main()
{
	std::cout << "Entering Inner scope\n"; // shows the scope
	{
		std::unique_ptr<Human> Casey = std::make_unique<Human>(); // creates a unique pointer
		Casey->name = "Casey"; // sets the humans name
		Casey->Talk(); // lets the human talk
	}
	std::cout << "Exiting Inner scope\n"; // shows the scope
	
	std::cout << "Entering Inner scope\n"; // shows the scope
	{
		std::unique_ptr<Human> Ely = std::make_unique<Human>(); // creates a unique pointer
		Ely->name = "Ely"; // sets the humans name
		Ely->Talk(); // lets the human talk
		Ely->Cry(); // lets the human cry
	}
	std::cout << "Exiting Inner scope\n"; // shows the scope
	
	std::cout << "\n\n"; // adds space

	std::cout << "Entering Outer scope\n"; // shows the scope
	{
		std::cout << "Entering Inner scope\n"; // shows the scope
		{
			std::shared_ptr<Human> sharedHuman2 = std::make_shared<Human>(); // creates a shared pointer
			
			sharedHuman2->name = "Mr.MeeSeeks"; // sets the humans name
			sharedHuman2->Talk(); // lets the human talk
			std::shared_ptr<Human> sharedHuman = sharedHuman2; // creates a shared pointer
			sharedHuman->Talk(); // lets the human talk
			sharedHuman->Cry();	 // lets the human cry
		}
		std::cout << "Exiting Inner scope\n"; // shows the scope
	}
	std::cout << "Exiting Outer scope\n"; // shows the scope
	
	std::cout << "\n\n"; // adds space
	
	std::cout << "Entering Outer scope\n"; // shows the scope
	{
		std::cout << "Entering Inner scope\n"; // shows the scope
		{
			std::weak_ptr<MissedInformation> weakHuman2 = std::make_shared<MissedInformation>(); // creates a weak pointer
			std::weak_ptr<MissedInformation> weakHuman = weakHuman2; // creates a weak pointer
		}
		std::cout << "Exiting Inner scope\n"; // shows the scope
	}
	std::cout << "Exiting Outer scope\n"; // shows the scope
}